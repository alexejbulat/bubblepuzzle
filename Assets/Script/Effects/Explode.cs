﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class Explode : MonoBehaviour {

    private const float LIFE_TIME = 2f;

	// Use this for initialization
	void Start () {

        StartCoroutine(Remove());
        this.particleSystem.Emit(10);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator Remove() {
        yield return new WaitForSeconds(LIFE_TIME);

        Destroy(this.gameObject);
    }
}
