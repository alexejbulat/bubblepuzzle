﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class BubbleController : MonoBehaviour {

    public Explode explodePrefab;

    public List<GameObject> topBubbles
    {
        get;
        set;
    }

    public int countLine
    {
        get;
        set;
    }

	// Use this for initialization
	void Start () {
        topBubbles = new List<GameObject>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FixLocation(GameObject obj, BubbleBehavior aroundBubble) {
        List<CellLocation> potentialPosition = aroundBubble.locationBubble.getAroundCellIndex();
        // found location
        CellLocation locationBall = null;
        float curentDistance = float.MaxValue;
        for (int i = 0; i < potentialPosition.Count; i++) {
            CellLocation location = potentialPosition[i];
            float newDistance = Vector3.Distance(obj.transform.position, location.Position);
            if (curentDistance > newDistance && isPositionFree(location.row, location.column)) {
                locationBall = location;
                curentDistance = newDistance;
            }
        }
        
        if (potentialPosition.Count == 0)
        {
            Debug.LogError("it s a trap");
        }
        
        if (locationBall != null) {
            obj.transform.position = locationBall.Position;
            BubbleBehavior behavior = obj.GetComponent<BubbleBehavior>();
            if (behavior != null) {
                behavior.locationBubble = locationBall;
            }
        }
        else {
            Debug.Log("position " + obj.transform.position + " around buble " + potentialPosition.Count);
        }
    }
    
    public List<GameObject> GetAroundObjectWithSameColor(GameObject compareObj, List<GameObject> arrayCheckBabbl) {
        List<GameObject> array = new List<GameObject>();
		foreach(GameObject bubbleObj in arrayCheckBabbl) {
            if (isEqualTypeBubbles(compareObj, bubbleObj) && isAroundGameObject(compareObj, bubbleObj)) {
                array.Add(bubbleObj);
            }
        }
        return array;
    }
    
    
    
    public bool isEqualTypeBubbles(GameObject gameObj, GameObject objectCollision) {
        BubbleBehavior behaviorCollisionObj = objectCollision.GetComponent<BubbleBehavior>();
        BubbleBehavior behaviorObj = gameObj.GetComponent<BubbleBehavior>();
        if (behaviorCollisionObj != null && behaviorObj != null) {
            return behaviorCollisionObj.typeBubble == behaviorObj.typeBubble;
        }
        return false;
    }
    
    public bool isAroundGameObject(GameObject obj1, GameObject obj2) {
        BubbleBehavior behavior1 = obj1.GetComponent<BubbleBehavior>();
        BubbleBehavior behavior2 = obj2.GetComponent<BubbleBehavior>();
        return behavior1.locationBubble.isAroundLocation(behavior2.locationBubble);
    }
    
    public bool isPositionFree(int row, int column) {
        foreach(GameObject obj in GameController.Instance.ContainerObject.topBubbles) {
            BubbleBehavior behavior = obj.GetComponent<BubbleBehavior>();
            if (behavior != null && behavior.locationBubble.row == row && behavior.locationBubble.column == column) {
                return false;
            }
        }
        return true;
    }


    public void ColsionWithBubble(GameObject obj, GameObject otheBubble) {
		BubbleBehavior ballBehgavior = obj.GetComponent<BubbleBehavior>();
		ballBehgavior.shooting = false;
        obj.rigidbody2D.velocity = Vector2.zero;
        //obj.rigidbody2D.gravityScale = 1;
        if (otheBubble != null) {
            FixLocation(obj, otheBubble.GetComponent<BubbleBehavior>());
        }
		AddBubleToContainer(ballBehgavior);
		List<GameObject>arrayDropObjects = null;
		bool isMultiColorBall = ballBehgavior.typeBubble == kTypeBubble.kMultiColorBubble;
		if(!isMultiColorBall) {
			arrayDropObjects = GetGroupObject(obj, topBubbles, true);
		}
		else {
			ballBehgavior.typeBubble = 0;
			arrayDropObjects = GetGroupObject(obj, topBubbles, true);
			for(int i = 1; i < (int)kTypeBubble.kMultiColorBubble; i++) {
				ballBehgavior.typeBubble = (kTypeBubble)i;
				List<GameObject>tempCheckColor = GetGroupObject(obj, topBubbles, true);
				if(tempCheckColor.Count > arrayDropObjects.Count) {
					arrayDropObjects = tempCheckColor;
				}
			}
		}
		if (arrayDropObjects != null && (arrayDropObjects.Count > 3 || isMultiColorBall)) {

            foreach(GameObject dropObject in arrayDropObjects) {
                StartCoroutine(CreateExplosion(dropObject.transform.position));
                StartCoroutine(GameController.Instance.score.ShowText(dropObject.transform.position));
                DestroyBall(dropObject);
            }
            List<GameObject> dropGroup = GetAllGroup();

			StartCoroutine(GameController.Instance.score.ShowComboText(arrayDropObjects.Count + dropGroup.Count));
        }
        GameController.Instance.Gun.bubble = null;
		ballBehgavior.collisionDelegate = null;
        GameController.Instance.timeEndShoot = Time.time;
        GameController.Instance.state = StateBubblePuzzle.kPrepareForShoot;
    }

    public GameObject CreateBubble() 
    {
        GameObject ball = (GameObject)GameObject.Instantiate(GameController.Instance.PrefabBall);
        ball.rigidbody2D.isKinematic = false;
        ball.transform.localScale = new Vector3(GameController.Instance.SizeBubble, GameController.Instance.SizeBubble, 1);
        return ball;
    }
	
	GameObject CreateBubbleWithoutType(int column, int row) {
		GameObject ball = CreateBubble();
		ball.layer = 12;
		BubbleBehavior behavior = ball.GetComponent<BubbleBehavior>();
		behavior.locationBubble = new CellLocation(row, column);
		behavior.willDestroy = DestroyBall;
		ball.transform.parent = transform;
		ball.transform.position = behavior.locationBubble.Position;
		return ball;
	}

	public void SetTypeBubble(GameObject ball, int typeBubble) {
		BubbleBehavior behavior = ball.GetComponent<BubbleBehavior>();
		if (behavior != null)
		{
			behavior.typeBubble = (kTypeBubble)typeBubble;
			SpriteRenderer spriteRender = ball.GetComponentInChildren<SpriteRenderer>();
			if(behavior.typeBubble != kTypeBubble.kMultiColorBubble) {
				spriteRender.sprite = GameController.Instance.spriteBubble[typeBubble];
			}
			else {
				spriteRender.sprite = GameController.Instance.multiColorSprite;
			}
		}
	}

    public void AddBubleToContainer(BubbleBehavior bubble) {
        List<GameObject> aroundObject = GetAroundBall(bubble.gameObject, topBubbles);
        BubbleBehavior behavior = bubble.GetComponent<BubbleBehavior>();

        foreach(GameObject obj in aroundObject) 
        {
            //CreateJoint( bubble.gameObject, obj.rigidbody2D);
            //GameController.Instance.changeColor(obj);
        }
        topBubbles.Add(bubble.gameObject);
		bubble.gameObject.layer = LayerMask.NameToLayer("AllBubleLayer");
        bubble.transform.parent = GameController.Instance.ContainerObject.transform;
        if (behavior != null) {
            behavior.willDestroy = DestroyBall;
        }
    }

    public void DestroyBall(GameObject obj) {
        topBubbles.Remove(obj);
		BubbleBehavior behavior = obj.GetComponent<BubbleBehavior>();
		behavior.willDestroy = null;
        Destroy(obj);
    }

    private IEnumerator CreateExplosion(Vector3 position) {
        yield return null;

        if (explodePrefab != null){
            GameObject createdObj = GameObject.Instantiate(explodePrefab, position, Quaternion.identity) as GameObject;

        }
    }
        
    public void CollisionDelegate(GameObject obj, GameObject collObj) {

        if (Utilities.isBubbleColision(obj, collObj)) {
            ColsionWithBubble(obj, collObj);
            //
            obj.GetComponent<BubbleBehavior>().ShootEnd();
        }
        else if(Utilities.isTopColision(obj, collObj))
        {
            Cell v2 = Utilities.IndFromPos( obj.transform.position );

            v2.r = 0;

            if(!isPositionFree(v2.r, v2.c))
            {
                ++v2.c;
            }
            if(!isPositionFree(v2.r, v2.c))
            {
                v2.c-=2;
            }

            obj.transform.position = Utilities.PosFromIndx(0, v2.c);
            obj.rigidbody2D.velocity = Vector2.zero;
            //obj.rigidbody2D.gravityScale = 1;
            //obj.rigidbody2D.isKinematic = true;
            
            BubbleBehavior beh = obj.GetComponent<BubbleBehavior>();
            beh.locationBubble = new CellLocation(0, v2.c);
            ColsionWithBubble(obj, null);
            obj.GetComponent<BubbleBehavior>().ShootEnd();
        }

    }
    
    public void DropBubbleDelegate(GameObject obj) {
        BubbleBehavior behavior = obj.GetComponent<BubbleBehavior>();
        if (behavior != null) {
            behavior.isFree();
        }
        topBubbles.Remove(obj);
    }

    public List<GameObject> AddLine(bool isEven) 
    {
        List<GameObject> line = new List<GameObject>();
        
        int countGo = isEven ? GameController.Instance.evenCount : GameController.Instance.unevenCount;
        
        for (int i = 0; i < countGo; i++) {
			GameObject ball = CreateBubbleWithoutType( i, countLine);

			int typeBubble = Random.Range(0, (int)kTypeBubble.kMultiColorBubble);
			SetTypeBubble(ball, typeBubble);

            topBubbles.Add(ball);
            line.Add(ball);
        }       
        countLine++;
        return line;
    }

	public List<GameObject> AddLine(string lineInfo) {
		List<GameObject> line = new List<GameObject>();
		char[] arrayType = lineInfo.ToCharArray();
		for(int i = 0; i < arrayType.Length; i++) {
			char symbol = arrayType[i];
			if(symbol == ' ') {
				continue;
			}
			GameObject ball = CreateBubbleWithoutType( i, countLine);
			topBubbles.Add(ball);
			line.Add(ball);

			int typeBubble = 0;
			switch(symbol) {
			case '1' : {
				typeBubble = 1;
				break;
				}
			case '2' : {
				typeBubble = 2;
				break;
				}
			case '3' : {
				typeBubble = 3;
				break;
				}
			}
			SetTypeBubble(ball, typeBubble);
		}       
		countLine++;
		return line;
	}

	public List<GameObject> GetAllGroup() {
        // create copy
        List<GameObject> copyAllBubbles = new List<GameObject>();
        foreach(GameObject obj in GameController.Instance.ContainerObject.topBubbles) {
            copyAllBubbles.Add(obj);
        }
        int countGroup = 0;
		List<GameObject> droppedObject = new List<GameObject>();
        while (copyAllBubbles.Count > 0 || countGroup < copyAllBubbles.Count) {
            GameObject startObject = copyAllBubbles[0];
            List<GameObject> groupObject = GetGroupObject(startObject, copyAllBubbles, false);
            // isGroup down
            if (Utilities.isGroupObjectDrop(groupObject)) {
                foreach(GameObject obj in groupObject) {
                    DropBubbleDelegate(obj);
					droppedObject.Add(obj);
				}
            }
            // remove checked object
            foreach(GameObject checkedObject in groupObject) {
                copyAllBubbles.Remove(checkedObject);
            }
            countGroup++;
        }
		return droppedObject;
    }

    public List<GameObject> GetAroundBall(GameObject obj, List<GameObject> topLineObjects) {
        List<GameObject> roundObject = new List<GameObject>();
        foreach (GameObject bubble in topLineObjects) {
            if (isAroundGameObject(obj, bubble)) {
                roundObject.Add(bubble);
            }
        }
        return roundObject;
    }


    public List<GameObject> GetGroupObject(GameObject startObjectGroup, List<GameObject> objectForChecking, bool withSameColor) {
        List<GameObject> returnArray = new List<GameObject>();
        returnArray.Add(startObjectGroup);
        // create copy array
        List<GameObject> copyCheckObject = new List<GameObject>();
        foreach(GameObject obj in objectForChecking) {
            if (!returnArray.Contains(obj)) {
                copyCheckObject.Add(obj);
            }
        }
        // geting group
        List<GameObject> tempCheckObject = returnArray;
        while(copyCheckObject.Count > 0) {
            List<GameObject> addedObject = new List<GameObject>();
            foreach(GameObject tempObj in tempCheckObject) {
				List<GameObject> aroundBubbles = withSameColor ? GetAroundObjectWithSameColor(tempObj, copyCheckObject) : GetAroundBall(tempObj, copyCheckObject);
                foreach (GameObject aroundBall in aroundBubbles) {
                    if (!returnArray.Contains(aroundBall)) {
                        copyCheckObject.Remove(aroundBall);
                        addedObject.Add(aroundBall);
                    }
                }
            }
            foreach(GameObject obj in addedObject) {
                returnArray.Add(obj);
            }
            if(addedObject.Count == 0) {
                break;
            }
            tempCheckObject = addedObject;
        }
        return returnArray;
    }

	public void ParseGameLvl(string path) {
		
        TextAsset asset = Resources.Load<TextAsset>(path);

        string line;
        StringReader theReader = new StringReader(asset.text);

		int maxCountInLine = -1;
		int countLine = 0;
		bool isMaxCountInEven = false;
		bool isMaxCountInUneven = false;

		using (theReader)
		{
			do
			{
				line = theReader.ReadLine();
				if(line != null) {
					if(line.Length >= maxCountInLine) {
						maxCountInLine = line.Length;
						if(!isMaxCountInEven) {
							isMaxCountInEven = (countLine%2) == 1;
						}
						if(!isMaxCountInUneven) {
							isMaxCountInUneven = (countLine%2) == 0;
						}
					}
					AddLine(line);
					countLine++;
				}
			}
			while (line != null);  
			theReader.Close();
		}
		GameController.Instance.countOfRows = countLine;

		GameController.Instance.CalculateSizeBubble(maxCountInLine, (isMaxCountInEven && isMaxCountInUneven));
		CorectLocationAndSizeBubble();
	}

	void CorectLocationAndSizeBubble() {
		foreach(GameObject ball in topBubbles) {
			ball.transform.localScale = new Vector3(GameController.Instance.SizeBubble, GameController.Instance.SizeBubble, 1);
			BubbleBehavior behavior = ball.GetComponent<BubbleBehavior>();
			if(behavior != null) {
				ball.transform.position = behavior.locationBubble.Position;
			}
		}
	}
}
