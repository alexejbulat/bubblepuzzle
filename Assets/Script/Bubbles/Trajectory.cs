﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trajectory : MonoBehaviour {

	public float sizeLine = 10;
	public GameObject prefab;
	public float distanceBetweenPoint = 1f;
	public List<GameObject> pull;

	void Start() {
		pull = new List<GameObject>();
		int countObjects = (int)Mathf.Round(sizeLine/distanceBetweenPoint);
		for(int i = 0; i < countObjects; i++) {
			GameObject point = (GameObject)GameObject.Instantiate(prefab) as GameObject;
			point.transform.parent = gameObject.transform;
			point.SetActive(false);
			pull.Add(point);
		}
		Debug.Log("count obj " + countObjects);
	}

	public void CalculateTrajectory() {
		foreach(GameObject obj in pull) {
			obj.SetActive(false);
		}
		
		Vector3 origin = transform.position;
		Vector3 direction = transform.up;
		float tempSizeTrajectory = 0;

		int layerBubblesMask = 1 << LayerMask.NameToLayer("AllBubleLayer");
		int layerGroundMask = 1 << LayerMask.NameToLayer("Ground");
		int finalmask = layerBubblesMask | layerGroundMask;

		int countVisiblePoint = 0;

		while(tempSizeTrajectory < sizeLine) {
			//create raycast
			Ray ray = new Ray(origin, direction);
			RaycastHit2D hit = Physics2D.Raycast( origin, direction, sizeLine - tempSizeTrajectory, finalmask);
			float distance = Vector2.Distance(hit.point, new Vector2(origin.x, origin.y));
			// fixing distance and get point end of ray
			if(hit.collider != null && 
			   !Utilities.isBubbleObject(hit.collider.gameObject) &&
			   !Utilities.isTopObject(hit.collider.gameObject)) {
				int offsetBall = direction.x > 0 ? 1 : -1;
				distance = ((hit.point.x - GameController.Instance.SizeBubble*offsetBall/2f) - origin.x)/direction.x;
			}
			if(distance + tempSizeTrajectory > sizeLine) {
				distance = sizeLine - tempSizeTrajectory;
			}
			origin = ray.GetPoint(distance);
			// added visible point
			while((countVisiblePoint + 1)*distanceBetweenPoint < (tempSizeTrajectory + distance)) {
				GameObject point = pull[countVisiblePoint];
				point.SetActive(true);
				float distanceForPoint = ((countVisiblePoint + 1)*distanceBetweenPoint - tempSizeTrajectory);
				point.transform.position = ray.GetPoint(distanceForPoint);

				countVisiblePoint++;
			}
			// prepare for reflact ray
			Vector3 inNormal = direction.x > 0 ? Vector3.right : Vector3.left;
			direction = Vector3.Reflect(direction, inNormal);
			tempSizeTrajectory += distance;
			// break from while
			if(hit.collider != null && (Utilities.isBubbleObject(hit.collider.gameObject) || Utilities.isTopObject(hit.collider.gameObject))) {
				break;
			}
		}
		//Debug.Log("distance " + tempSizeTrajectory);
	}
}
