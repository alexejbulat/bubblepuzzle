﻿//#define Debug_Around_Cell

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum kTypeBubble {
	kRedBubble,
	kBlueBubble,
	kGreenBubble,
	kWhiteBubble,
	kMultiColorBubble,
	kCountTypeBubble
};

public delegate void CollisionDelegate(GameObject obj, GameObject collObj);
public delegate void GameObjectDelegate(GameObject obj);

public class BubbleBehavior : MonoBehaviour {
	public bool isDestroy = false;
    public bool shooting = false;
    public bool falling = false;
	public static string bubbleTag = "bubble";
	public static string TopTag = "topBounce";
	public GameObjectDelegate willDestroy;
	public CollisionDelegate collisionDelegate;

	public kTypeBubble typeBubble;

	public CellLocation locationBubble;

	bool preTestVal;
	public bool testVal;
	// Use this for initialization
	void Start () {
		isDestroy = preTestVal = testVal = false;
	}
	
    void FixedUpdate()
    {
        return;
        if (locationBubble == null)
        {
            return;
        }

        Vector2 dir = (locationBubble.Position - transform.position);

        transform.position = locationBubble.Position;

        if (this.rigidbody2D.velocity.magnitude < 1.5f )
        {
            transform.position = locationBubble.Position;
            this.rigidbody2D.velocity = Vector2.zero;
        } else if (!falling && !shooting)
        {

            this.rigidbody2D.AddForce(dir * 2500);
            //
        } 

        if (Vector3.Distance(transform.position, locationBubble.Position) > GameController.Instance.SizeBubble)
        {
            this.rigidbody2D.velocity = dir;
        }
    }

	// Update is called once per frame
	void Update () {
		if (transform.position.y < (GameController.Instance.gameRect.yMax - GameController.Instance.gameRect.height * GameController.GameOverLineLocation) && willDestroy != null)
        {
            if (!falling)
            {
                GameController.Instance.GameOver();
            }
        }
        if (preTestVal != testVal)
        {
            Debug.Log("row " + locationBubble.row + " column " + locationBubble.column);
            preTestVal = testVal;
        }

        if (locationBubble != null)
        {
            transform.position = locationBubble.Position;
        }
    }

    public void OnCollisionEnter2D(Collision2D coll) {
		checkCollisionBettwenBubble(coll);
		int topBubbleLayer = LayerMask.NameToLayer("DropBubbleLayer");
		if (coll.collider.gameObject.name == "Bottom" && !isDestroy && gameObject.layer == topBubbleLayer)
        {
			isDestroy = true;
			StartCoroutine(GameController.Instance.score.ShowText(this.transform.position));
			StartCoroutine(DestroyWithDelay());
        }
	}

    public void checkCollisionBettwenBubble (Collision2D coll) {
		if (collisionDelegate != null) {
			collisionDelegate(gameObject, coll.gameObject);
		}
	}

    public void ShootEnd()
    {
        shooting = false;

        TrailRenderer.Destroy(this.GetComponentInChildren<TrailRenderer>());
    }

    public void ShooyStart()
    {
        shooting = true;
        TrailRenderer trail = this.GetComponentInChildren<TrailRenderer>();
        trail.startWidth = 1;
        trail.endWidth = 0;

        Texture2D tex = GetComponentInChildren<SpriteRenderer>().sprite.texture;

        Color color = tex.GetPixel(tex.width / 2, tex.height / 2);

        //Debug.Log(color);
        trail.material.SetColor("_TintColor",   color);
    }

    public void isFree()
    {
		this.gameObject.layer = LayerMask.NameToLayer("DropBubbleLayer");
		SpriteRenderer spriteRender = this.gameObject.GetComponentInChildren<SpriteRenderer>();
		spriteRender.sortingLayerName = "DropBubbleLayer";

        falling = true;
        locationBubble = null;
        rigidbody2D.gravityScale = 1;

        float mult = 1000;
        float a = Random.Range(-mult, mult);

        rigidbody2D.AddForce(new Vector2(a, 0));
    }

    private IEnumerator DestroyWithDelay()
    {
        yield return new WaitForSeconds(1);

        if(willDestroy != null)
            willDestroy(gameObject);
    }


#if Debug_Around_Cell

	public GameObjectDelegate aroundObjectDelegate;
	public GameObjectDelegate endAroundObjectDelegate;

	void OnMouseEnter() {
		if (aroundObjectDelegate != null)
			aroundObjectDelegate(gameObject);
	}

	void OnMouseExit() {
		if (endAroundObjectDelegate != null)
			endAroundObjectDelegate(gameObject);
	}
#endif
}
