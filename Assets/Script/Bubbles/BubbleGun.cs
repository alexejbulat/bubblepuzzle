﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BubbleGun : MonoBehaviour {

    public GameObject bubble
    {
        get;
        set;
    }

    private GameObject nextBubble = null;
	public Trajectory trajectory;
	public GameObject tempPositionBubble;
	public GameObject tempPositionNextBubble;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	public void UpdateBubleGun () {

		Vector3 mouse = Input.mousePosition;
		mouse.z = 0;
		mouse.x -= Screen.width / 2;
		mouse = mouse.normalized;
		
		if (Mathf.Abs(Vector3.Angle(Vector3.up, mouse)) < 60)
		{
			this.transform.rotation = Quaternion.FromToRotation(Vector3.up, mouse);
		}
		trajectory.CalculateTrajectory();

		if (bubble != null) {
			bubble.transform.position = tempPositionBubble.transform.position;
			bubble.transform.rotation = tempPositionBubble.transform.rotation;
		}

		if (nextBubble != null) {
			nextBubble.transform.position = tempPositionNextBubble.transform.position;
			nextBubble.transform.rotation = tempPositionNextBubble.transform.rotation;
		}
	}

	void Update() {

	}

    public void PrepareForShoot () 
    {
        Debug.Log("prepare for shoot");

        if (nextBubble == null)
        {
            nextBubble = createShootableBubble();
        }

        bubble = nextBubble;

        nextBubble = createShootableBubble();

        if (!CanBePrepared(bubble.GetComponent<BubbleBehavior>().typeBubble))
        {
            GameObject.DestroyImmediate(bubble);
            bubble = createShootableBubble();
        }
    }

    private GameObject createShootableBubble()
    {
        GameObject ball = GameController.Instance.ContainerObject.CreateBubble();
        
        List<kTypeBubble> allPosibleColor = new List<kTypeBubble>();
        foreach(GameObject obj in GameController.Instance.ContainerObject.topBubbles) {
            BubbleBehavior behavior = obj.GetComponent<BubbleBehavior>();
            if (!allPosibleColor.Contains(behavior.typeBubble)) {
                allPosibleColor.Add(behavior.typeBubble);
            }
        }
        int indexBubble = Random.Range(0, allPosibleColor.Count);
        //kTypeBubble typeBubble = allPosibleColor[indexBubble];
        float chanceMultiColor = Random.value;
        kTypeBubble typeBubble = chanceMultiColor > GameController.Instance.multiColorChance ? allPosibleColor[indexBubble] : kTypeBubble.kMultiColorBubble;

		GameController.Instance.ContainerObject.SetTypeBubble(ball, (int)typeBubble);

		ball.rigidbody2D.isKinematic = false;
		ball.rigidbody2D.gravityScale = 0;
		return ball;
    }

    private bool CanBePrepared(kTypeBubble type)
    {
        if (type == kTypeBubble.kMultiColorBubble)
        {
            return true;
        }

        List<kTypeBubble> allPosibleColor = new List<kTypeBubble>();
        foreach(GameObject obj in GameController.Instance.ContainerObject.topBubbles) {
            BubbleBehavior behavior = obj.GetComponent<BubbleBehavior>();
            if (!allPosibleColor.Contains(behavior.typeBubble)) {
                allPosibleColor.Add(behavior.typeBubble);
            }
        }

        if (allPosibleColor.Contains(type))
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void ShootBall () {
        if (bubble == null) {
            Debug.Log("bubble is null");
            return;
        }
		Debug.Log("shoot ball " + this.transform.up);
        bubble.GetComponent<BubbleBehavior>().ShooyStart();
		bubble.rigidbody2D.AddForce(this.transform.up*2500);
        BubbleBehavior behavior = bubble.GetComponent<BubbleBehavior>();
        if (behavior != null) {
            behavior.collisionDelegate = GameController.Instance.ContainerObject.CollisionDelegate;
        }
		bubble = null;
    }
}
