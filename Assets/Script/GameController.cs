﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum StateBubblePuzzle {
	kIdleSate,
	kShootState,
	kPrepareForShoot,
	kGameOverState,
	kWinState
}


public class GameController : MonoBehaviour {

	private static GameController instanse;

	public ScoreController score;
    public BubbleGun Gun;
    public float speedFalling;
    public GameObject backGround;
    public Rect gameRect;
    public GameObject TopBounce
    {
        get;
        set;
    }
    public int evenCount;
    public int unevenCount;
    public int countOfRows;

    public BubbleController ContainerObject;
	public GameObject PrefabBall;

	public List<Sprite> spriteBubble;
	public Sprite multiColorSprite;
	public float multiColorChance = 0.5f;
	public static float GameOverLineLocation = 0.8f;

	public float timeEndShoot
    {
        get;
        set;
    }
	
    public StateBubblePuzzle state
    {
        get;
        set;
    }

    public float HeightRow
    {
        get;
        set;
    }
    
    public float SizeBubble
    {
        get;
        set;
    }

	public GameObject GameOverLine;

	void Awake()
    {
		instanse = this;
        state = StateBubblePuzzle.kIdleSate;
        timeEndShoot = float.MaxValue;

		CalculateSizeBubble(Mathf.Max(evenCount, unevenCount), (evenCount == unevenCount));

        GameObject top = null;
        GameObject bottom = null;
        GameObject left = null;
        GameObject right = null;
        
        for (int i = 0; i < backGround.transform.childCount; ++i)
        {
            Transform child = backGround.transform.GetChild(i);
            if(child.gameObject.name == "Top")
            {
                top = child.gameObject;
            } else if(child.gameObject.name == "Bottom")
            {
                bottom = child.gameObject;
            } else if(child.gameObject.name == "Left")
            {
                left = child.gameObject;
            } else if(child.gameObject.name == "Right")
            {
                right = child.gameObject;
            }
        }
        
        top.transform.localScale = new Vector3(gameRect.width, 1, 1);
        top.transform.position = new Vector3(gameRect.center.x, gameRect.yMax + 0.5f,0);
		TopBounce = top;
        
        bottom.transform.localScale = new Vector3(gameRect.width, 1, 1);
        bottom.transform.position = new Vector3(gameRect.center.x, gameRect.yMin,0);
        
        left.transform.localScale = new Vector3(1, gameRect.height, 1);
        left.transform.position = new Vector3(gameRect.xMin - 0.5f, gameRect.center.y, 0);
        
        right.transform.localScale = new Vector3(1, gameRect.height, 1);
        right.transform.position = new Vector3(gameRect.xMax + 0.5f, gameRect.center.y, 0);

		for(int i = 0; i < spriteBubble.Count; i++) {
			Sprite sprite = spriteBubble[i];
			spriteBubble.RemoveAt(i);
			int newIndex = Random.Range(0, spriteBubble.Count - 1);
			spriteBubble.Insert(newIndex, sprite);
		}
	}

	// Use this for initialization
	void Start () 
    {
		bool isParseLvl = true;

		if(isParseLvl) {
			string name = "gameLvl";
            ContainerObject.ParseGameLvl(name);
		}
		else {
			int countLines = countOfRows;
			for (int j = 0; j < countLines; j++) 
			{
				ContainerObject.AddLine(j % 2 == 1);
			}
		}
		Gun.transform.localScale *= GameController.Instance.SizeBubble;
		Gun.transform.position = new Vector3(gameRect.center.x, gameRect.yMin,0);
		Gun.PrepareForShoot();
		GameOverLine.transform.position = Vector3.up*(gameRect.yMax - gameRect.height * GameOverLineLocation - SizeBubble/2f);
	}

    void OnGUI() 
    {
        if (state == StateBubblePuzzle.kGameOverState)
        {
            GameOverDialog.DrawGameOverDialog(
                ()=>
                {
                Application.LoadLevel(Application.loadedLevel);
            }
            );
        }

		if (state == StateBubblePuzzle.kWinState)
		{
			GameOverDialog.DrawWinDialog(
				()=>
				{
				Application.LoadLevel(Application.loadedLevel);
			}
			);
		}
    }
	
	void Update () {

		if (ContainerObject.topBubbles.Count == 0) {
			state = StateBubblePuzzle.kWinState;
		}

		if (state == StateBubblePuzzle.kGameOverState || state == StateBubblePuzzle.kWinState)
        {
            return;
        }

		Gun.UpdateBubleGun();

        ContainerObject.transform.position += Vector3.down * Time.deltaTime * speedFalling;
		TopBounce.transform.position = new Vector3(gameRect.center.x, gameRect.yMax + 0.5f + ContainerObject.transform.position .y,0);

		if (Input.GetMouseButtonUp(0) && state == StateBubblePuzzle.kIdleSate) {
			Gun.ShootBall();
		}
		if (Time.time - timeEndShoot > 0.3 && state == StateBubblePuzzle.kPrepareForShoot) {
            Gun.PrepareForShoot();
			timeEndShoot = float.MaxValue;
			state = StateBubblePuzzle.kIdleSate;
		}
	}
	
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(new Vector3(gameRect.center.x, gameRect.center.y, 0), new Vector3(gameRect.width, gameRect.height, 1));
        Gizmos.color = Color.white;
    }

    public void GameOver()
    {
		state = StateBubblePuzzle.kGameOverState;
    }

	public static GameController Instance
	{
		get
		{
			return instanse;
		}
	}

	public void CalculateSizeBubble(int countBallInLine, bool withOffset) {
		if (withOffset)
		{
			SizeBubble = GameController.Instance.gameRect.width / (countBallInLine + 0.5f);
		} 
		else
		{
			SizeBubble = GameController.Instance.gameRect.width / countBallInLine;
		}
		HeightRow = GameController.Instance.SizeBubble * 0.75f / Mathf.Cos(Mathf.PI / 6);
		Debug.Log("calculated size " + SizeBubble);
	}

#region Debug Section

	public void changeColor(GameObject obj) {
        List<GameObject> aroundBalls = ContainerObject.GetAroundBall(obj, ContainerObject.topBubbles);
		foreach(GameObject testObj in aroundBalls) {
			BubbleBehavior behavior = testObj.GetComponent<BubbleBehavior>();
			if (behavior != null)
			{
				SpriteRenderer spriteRender = testObj.GetComponentInChildren<SpriteRenderer>();
				spriteRender.sprite = spriteBubble[4];
			} 
		}
	}

    public void endChangeColor(GameObject obj)
	{
        List<GameObject> aroundBalls = ContainerObject.GetAroundBall(obj, ContainerObject.topBubbles);
		foreach(GameObject testObj in aroundBalls) {
			BubbleBehavior behavior = testObj.GetComponent<BubbleBehavior>();
			if (behavior != null)
			{
				SpriteRenderer spriteRender = testObj.GetComponentInChildren<SpriteRenderer>();
				spriteRender.sprite = spriteBubble[(int)behavior.typeBubble];
			} 
		}
	}
#endregion
}

