using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CellLocation {
	public readonly int row;
	public readonly int column;
	public Vector3 Position {
		get {
			return Utilities.PosFromIndx(row, column);
		}
	}

	public CellLocation(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public bool isAroundLocation(CellLocation aroundObject) {
		// horizontal check
		if (row == aroundObject.row && Mathf.Abs(column - aroundObject.column) == 1) {
			return true;
		}
		// vertical check
		int distance = (column - aroundObject.column);
		int checkedColumntDistance = (row%2 == 0) ? 1 : -1;
		if (Mathf.Abs(row - aroundObject.row) == 1 && (distance == checkedColumntDistance || distance == 0)) {
			return true; 
		}
		return false;
	}

	public List<CellLocation> getAroundCellIndex() {
		List<CellLocation> cellIndexs = new List<CellLocation>();
		int checkedColumntDistance = (row%2 == 0) ? 1 : -1;
		int verticalLeftColumn = column;
		int	verticalRightColumn = column - checkedColumntDistance;
		// left verticall
		if (Utilities.inBounds(row - 1, verticalLeftColumn)) {
			cellIndexs.Add(new CellLocation(row - 1, verticalLeftColumn));
		}
		if (Utilities.inBounds(row + 1, verticalLeftColumn)) {
			cellIndexs.Add(new CellLocation(row + 1, verticalLeftColumn));
		}
		// right verticall
		if (Utilities.inBounds(row - 1, verticalRightColumn)) {
			cellIndexs.Add(new CellLocation(row - 1, verticalRightColumn));
		}
		if (Utilities.inBounds(row + 1, verticalRightColumn)) {
			cellIndexs.Add(new CellLocation(row + 1, verticalRightColumn));
		}
		// horizontal
		if (Utilities.inBounds(row, column - 1)) {
			cellIndexs.Add(new CellLocation(row, column - 1));
		}
		if (Utilities.inBounds(row, column + 1)) {
			cellIndexs.Add(new CellLocation(row, column + 1));
		}
		return cellIndexs;
	}
}