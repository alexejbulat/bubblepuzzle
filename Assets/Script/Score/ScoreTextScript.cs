﻿using UnityEngine;
using System.Collections;

public class ScoreTextScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		Destroy(this.gameObject, Random.Range(1f, 2f));
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position += Vector3.up*0.5f*Time.deltaTime;
	}
}
