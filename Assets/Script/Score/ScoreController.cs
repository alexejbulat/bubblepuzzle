﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreController : MonoBehaviour {

	public static float topPercentForScore = 0.06f;
	public static float comboSplashPercent = 0.05f;
	public static float dropCofixient = 1.5f;

	public GameObject comboSplash;

	public GameObject topTextObj;

	public GameObject scoreTextPrefab;
	
	public GUIStyle styleComboLabel = null;

	int CountComboPoint = 0;
	public Sprite[] comboTexture;

	float score = 0;
	int ballValue = 500;
	// Use this for initialization
	void Start () {
		topTextObj.transform.position = new Vector3(0.5f, 1 - topPercentForScore/2f);
	}
	
	// Update is called once per frame
	void Update () {
		GUIText guiText = topTextObj.guiText;
		guiText.text = "SCORE:" + score;
		guiText.fontSize = (int)(topPercentForScore*Screen.height/dropCofixient);
	}

	float ComboCoficient(int countCombo) {
		float combo = 1f;
		if (countCombo == 4) {
			combo = 1.5f;
		}
		else if(countCombo == 5) {
			combo = 1.75f;
		}
		else if(countCombo == 6) {
			combo = 2f;
		}
		else if(countCombo == 7) {
			combo = 2.5f;
		}
		else if(countCombo == 8) {
			combo = 3f;
		}
		else if(countCombo == 9) {
			combo = 4f;
		}
		else if(countCombo > 9) {
			combo = 5f;
		}
		return combo;
	}

	public IEnumerator ShowComboText(int countCombo) {
		yield return new WaitForSeconds(0.2f);
		float comboCoficient = ComboCoficient(countCombo);
		if(comboCoficient > 1) {
			yield return new WaitForSeconds(0.2f);
			CountComboPoint = countCombo;
			float comboScore = (comboCoficient - 1)*countCombo*ballValue;
			score += comboScore;
			comboSplash.SetActive(true);
			int indexComboImage = CountComboPoint > 10 ? 6 : CountComboPoint - 4;

			SpriteRenderer spriteRender = comboSplash.GetComponentInChildren<SpriteRenderer>();
			spriteRender.sprite = comboTexture[indexComboImage];

			TextMesh textMesh = comboSplash.GetComponentInChildren<TextMesh>();
			textMesh.text = "+"+comboScore.ToString();

			MeshRenderer meshRender = comboSplash.GetComponentInChildren<MeshRenderer>();
			meshRender.sortingLayerName = "ComboSplash";
			yield return new WaitForSeconds(1f);
			CountComboPoint = 0;
			comboSplash.SetActive(false);
		}
	}

	public IEnumerator ShowText(Vector3 positionText) {
		yield return new WaitForSeconds(0.2f);
		GameObject textObj = Instantiate(scoreTextPrefab) as GameObject;
		textObj.transform.parent = gameObject.transform;
		textObj.transform.position = positionText + Vector3.up * Random.Range(0f, 1.2f);
		yield return new WaitForSeconds(0.2f);
		score += ballValue;
	}

	public void ResetScrore() {
		score = 0;
	}

	void OnGUI() {
		return;
		if(CountComboPoint > 0) {
			DrawComboScreen();
		}
	}

	void DrawComboScreen() {

//		Rect comboSplashRect = new Rect((Screen.width - width)/2.0f, (Screen.height - height)/2.0f, width, height);
//		GUI.DrawTexture(comboSplashRect, comboSplash);
//
//		float scale = (float)height/(float)comboSplash.height;
		// combo coficient
//		int comboCoffixient = CountComboPoint > 10 ? 6 : CountComboPoint - 4;
//		Sprite textureComboCoficient = comboTexture[comboCoffixient];
//
//		float width = 0;
//		float height = 0;
//		if(Screen.height > Screen.width) {
//			width = Screen.width*comboSplashPercent;
//			height = width*textureComboCoficient.height/textureComboCoficient.width;
//		}
//		else {
//			height = Screen.height*comboSplashPercent;
//			width = height*textureComboCoficient.width/textureComboCoficient.height;
//		}
//
//		Rect coficientImageRect = new Rect((Screen.width - width)/2.0f, Screen.height*0.44f - height/2.0f, width, height);
//		GUI.DrawTexture(coficientImageRect, textureComboCoficient);
//
//		// bonus score
//		float comboScore = (CountComboPoint - 1)*CountComboPoint*ballValue;
//		float heightLabel = 40*(int)width/textureComboCoficient.width;
//		Rect labelRect = new Rect(0, 
//		                          Screen.height*0.55f - heightLabel, 
//		                          Screen.width, 
//		                          heightLabel);
//		styleComboLabel.fontSize = 80*(int)width/textureComboCoficient.width;
//		GUI.Label(labelRect, "+" + comboScore.ToString(), styleComboLabel);
	}
}
