﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Rect gameRect = VisibleRect();
		camera.transform.LookAt(new Vector3(gameRect.xMax - gameRect.width/2f, gameRect.yMax - gameRect.height/2f, 0));
	}
	
	// Update is called once per frame
	void Update () {

		Rect gameRect = VisibleRect();
		camera.transform.LookAt(new Vector3(gameRect.xMax - gameRect.width/2f, gameRect.yMax - gameRect.height/2f, 0));
		float heightContent = gameRect.height;// + Screen.height*ScoreController.topPercentForScore;
		float gameAscpect = gameRect.width/heightContent;
		if(gameAscpect > camera.aspect) {
			camera.orthographicSize = gameRect.width/(2*camera.aspect);
		}
		else {
			camera.orthographicSize = heightContent/2f;
		}
	}

	Rect VisibleRect() {
		Rect gameRect = GameController.Instance.gameRect;
		float scoreOffset = gameRect.height*ScoreController.topPercentForScore;
		gameRect = new Rect(gameRect.x, gameRect.y, gameRect.width, gameRect.height + scoreOffset);
		return gameRect;
	}
}
