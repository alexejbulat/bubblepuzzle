
using System;
using UnityEngine;

public class GameOverDialog
{
    public static void DrawGameOverDialog(Action restart)
    {
		DrawDialogWithText("Game Over", restart);
    }

	public static void DrawWinDialog(Action restart)
	{
		DrawDialogWithText("Congratulation, You won!!!", restart);
	}

	static void DrawDialogWithText(string text, Action action) {
		float heigth = 100;
		float width = 200;
		
		Rect boxRect = new Rect((Screen.width - width) / 2, (Screen.height - heigth) / 2, width, heigth);
		GUI.Box(boxRect , "");
		
		float lableMargin = 20;
		float lableWidth = 100;
		float lableHigth = 40;
		
		Rect labRect = new Rect((Screen.width - lableWidth) / 2 , (Screen.height - lableHigth) / 2 - lableMargin, lableWidth, lableHigth);
		GUI.Label(labRect, text);
		
		Rect butRect = new Rect((Screen.width - lableWidth) / 2 , (Screen.height - lableHigth) / 2 + lableMargin, lableWidth, lableHigth);
		if (GUI.Button(butRect, "Ok"))
		{
			action.Invoke();
		}
	}
}


