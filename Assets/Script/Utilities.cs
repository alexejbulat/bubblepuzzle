
using UnityEngine;
using System.Collections.Generic;

public struct Cell
{
    public int r;
    public int c;
}

public class Utilities
{
    private static string bubbleTag = "bubble";
	private static string TopTag = "topBounce";

    static public Vector3 PosFromIndx(int r, int c)
    {
        float delta = GameController.Instance.ContainerObject.transform.position.y;
        float y = r * GameController.Instance.HeightRow + GameController.Instance.SizeBubble / 2;
        float x = (r % 2 != 0) ? ((c + 0.5f) * GameController.Instance.SizeBubble) : (c * GameController.Instance.SizeBubble);

        x += GameController.Instance.SizeBubble / 2;

        return new Vector3(GameController.Instance.gameRect.xMin + x, GameController.Instance.gameRect.yMax - y + delta, 0);
    }

	static public Cell IndFromPos(Vector3 pos)
    {
        float delta = GameController.Instance.ContainerObject.transform.position.y;

        float x = pos.x - GameController.Instance.gameRect.xMin;
        float y = GameController.Instance.gameRect.yMax - pos.y + delta - GameController.Instance.SizeBubble / 2;

        int r = (int)Mathf.Round(y /  GameController.Instance.HeightRow);
        int c = (r % 2 != 0) ? (int)Mathf.Round((x - 0.5f) / GameController.Instance.SizeBubble) 
            : (int)Mathf.Round(x / GameController.Instance.SizeBubble);

        Cell cell;
        cell.r = r;
        cell.c = c;
        return cell;
    }

	public static bool isBubbleColision(GameObject obj1, GameObject obj2) {
        return (obj1.CompareTag(bubbleTag) && obj2.CompareTag(bubbleTag));
    }

	public static bool isBubbleObject(GameObject obj) {
		return obj.CompareTag(bubbleTag);
	}
    
	public static bool isTopColision(GameObject obj1, GameObject obj2) {
        return (obj1.CompareTag(bubbleTag) && obj2.CompareTag(TopTag));
    }

	public static bool isTopObject(GameObject obj) {
		return obj.CompareTag(TopTag);
	}

	public static bool isGroupObjectDrop(List<GameObject> group) {
		foreach(GameObject obj in group) {
			/*if(obj.rigidbody2D.isKinematic) {
				return false;
			}*/
            //if(obj.transform.position.y > GameController.Instance.TopBounce.transform.position.y - GameController.Instance.SizeBubble)
            if(obj.GetComponent<BubbleBehavior>().locationBubble.row == 0)
            {
                return false;
            }
		}

		return true;
	}

    public static bool inBounds(int r, int c)
    {
        return r > 0 && (GameController.Instance.gameRect.Contains(PosFromIndx(r,c)));
    }
}

